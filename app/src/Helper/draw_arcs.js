import { MapboxLayer } from '@deck.gl/mapbox';
import { ArcLayer, ScatterplotLayer } from '@deck.gl/layers';


export default {
  DrawArcs(deplacement_data, centroides_data, map) {
    const movesInfos = this.buildMoveInfos(deplacement_data);
    const streaminfos = this.buildMobilityInfos(movesInfos, centroides_data);
    return this.displayMobilityInfos(streaminfos, map);
  },

  displayMobilityInfos(mobilityinfos, map) {
    const SOURCE_COLOR = [166, 3, 3];
    const TARGET_COLOR = [35, 181, 184];

    // When a mousemove event occurs on a feature in the places layer, display
    // arcs under target position and source position

    const arcsLayer = new MapboxLayer({
      type: ArcLayer,
      id: 'arcs',
      data: mobilityinfos.moves,
      brushRadius: 1000,
      getStrokeWidth: 1,
      opacity: 1,
      getSourcePosition: d => d.source,
      getTargetPosition: d => d.target,
      getSourceColor: SOURCE_COLOR,
      getTargetColor: TARGET_COLOR,
      onHover: info => this.setTooltip(info.object, info.x, info.y)
    });

    const centroidsLayer = new MapboxLayer({
      type: ScatterplotLayer,
      id: 'centroids',
      data: mobilityinfos.centroids,
      opacity: 1,
      pickable: true,
      getRadius: d => d.radius,
      getColor: TARGET_COLOR
    });

    map.addLayer(arcsLayer);
    map.addLayer(centroidsLayer);
  },
  // Create and return a object streaminfos with 2 arrays, centroids and moves
  buildMobilityInfos(moveinfos, centroides) {
    const streaminfos = { centroids: [], moves: [] };
    let listId = {};
    const centroidexists = centroid => {
      streaminfos.centroids.find(c => c.id === centroid.id);
    };

    moveinfos.forEach(miRecord => {
      /* Take postion [x,y,z=0] */
      const target_sect = miRecord.target.substr(0, 2);
      const source_sect = miRecord.source.substr(0, 2);
      const source_infos = this.getInfoSecteur(
        source_sect,
        centroides.features
      );
      const target_infos = this.getInfoSecteur(
        target_sect,
        centroides.features
      );

      listId = this.fillInList(listId, source_sect);
      const radius = this.setRadiusPropreties(listId[source_sect]);

      /* Mettre au propre & Crée une fonction spé*/

      if (target_infos && source_infos) {
        const centroid = {
          name: source_infos.nom,
          position: source_infos.coordonnee,
          id: source_infos.id,
          radius: radius
        };
        if (!centroidexists(source_infos.id)) {
          streaminfos.centroids.push(centroid);
        }

        streaminfos.moves.push({
          name: source_infos.name + '->' + target_infos.name,
          target: target_infos.coordonnee,
          source: source_infos.coordonnee
        });
      }
    });
    return streaminfos;
  },

  // Count nomber of source who have same centroids
  // return a objet {01: 669, 02: 1796, 03: 585 ....}
  fillInList(listId, source_sect) {
    let data_id = source_sect;
    const arrayIds = Object.keys(listId);
    if (arrayIds.length === 0 || !arrayIds.includes(data_id)) {
      listId[data_id] = 0;
    }
    listId[data_id] += 1;

    return listId;
  },

  buildMoveInfos(deplacement_data) {
    return deplacement_data.map(d => ({
      source: d.D3,
      target: d.D7,
      tirage: d.TIRA
    }));
  },

  /* Prend les secteur id les revois en coord */
  getInfoSecteur(secteur_id, features) {
    const int_id = parseInt(secteur_id, 10);
    const line = features.find(el => el.properties.EMD_ZTIR_2 == int_id);
    if (!line) {
      return false;
    }
    return {
      coordonnee: [...line.geometry.coordinates, 0],
      nom: line.properties.nom_com,
      id: secteur_id
    };
  },

  /* Specify the size of the radius with the nombre of source*/
  setRadiusPropreties(CentroidId) {
    if (CentroidId < 400) {
      return 400;
    } else if (CentroidId < 1000) {
      return CentroidId;
    } else if (CentroidId > 1000) {
      return 1000;
    } else {
      return 300;
    }
  },
  // On hover display a tooltip
  setTooltip(object, x, y) {
    const el = document.getElementById('tooltip');
    if (object) {
      el.innerHTML = object.centroids.name;
      el.style.display = 'block';
      el.style.left = x;
      el.style.top = y;
    } else {
      el.style.display = 'none';
    }
  }
};
