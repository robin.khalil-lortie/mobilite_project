import Vue from 'vue';
import Vuetify from 'vuetify';

import './style/style.scss';

import Mobilite from '@/components/Mobilite.vue';

const MobiliteApp = {
  initUI(/* app */) {
    Vue.use(Vuetify);
    this.vm = new Vue({
      /*events:{
           mousemove:
      }
*/

      components: {
        Mobilite
      },
      render: h => h(Mobilite, { props: { app: this } })
    }).$mount('#mobilite-mntpt');
  },

  exitUI(/* app */) {
    this.vm.$destroy();
  }
};

export default MobiliteApp;
