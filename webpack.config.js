const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const manifest = require('./app/app.json');
const appcode = manifest.appcode;
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const copyfiles = {
  root: ['app.json', 'placeholders/'],
  assets: ['mapdefs/', 'logo/', 'data/']
};

const copyconf = Object.keys(copyfiles).reduce((acc, k) => {
  let nacc = acc;
  if (k === 'root') {
    const dests = copyfiles[k];
    const rootDests = dests.map(el => {
      const dest = el.slice(-1) === '/' ? appcode + '/' + el : appcode;
      return { from: 'app/' + el, to: dest };
    });
    nacc = nacc.concat(rootDests);
  }
  if (k === 'assets') {
    const assets = copyfiles[k];
    const assetDests = assets.map(el => {
      const dest =
        el.slice(-1) === '/' ? appcode + '/assets/' + el : appcode + '/assets';
      return { from: 'app/' + el, to: dest };
    });
    nacc = nacc.concat(assetDests);
  }
  return nacc;
}, []);

module.exports = (env, args) => ({
  entry: {
    [appcode]: path.resolve(__dirname, 'app/src/index.js')
  },

  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: '[name]/assets/js/[name]-bundle[hash].js',
    library: 'eegle_apps/' + appcode,
    libraryTarget: 'umd',
    libraryExport: 'default'
  },
  devtool: args.mode === 'development' ? 'eval-source-map' : false,

  resolve: {
    modules: ['app/src', 'node_modules'],
    alias: {
      vue:
        args.mode === 'production' ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js',
      vuetify:
        args.mode === 'production'
          ? 'vuetify/dist/vuetify.min.js'
          : 'vuetify/dist/vuetify.js',
      '@': path.resolve(__dirname, 'app/src')
    }
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: file => /node_modules/.test(file) && !/\.vue\.js/.test(file)
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: 'vue-style-loader'
          },
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[local]'
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.(c|d|t)sv$/, // load all .csv, .dsv, .tsv files with dsv-loader
        use: ['dsv-loader']
      },
      {
        test: /\.styl$/,
        use: ['stylus-loader', 'css-loader']
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      }
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name]/assets/css/style_[hash].css'
    }),
    new CopyWebpackPlugin(copyconf),
    new VueLoaderPlugin()
  ]
});
